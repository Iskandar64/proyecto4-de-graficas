#pragma once

#include "scene.h"
#include "mat4.h"

// Escena de prueba para comenzar a trabajar con
// fragment shaders.
class scene_cube : public scene
{
public:
	~scene_cube();
	//float w;
	//float h;

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key) { }
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	//cgmath::mat4 matrizTransformacion();

private:
	GLuint shader_program;

	GLuint vao;
	GLuint positionsVBO;
	GLuint indicesBuffer;
	GLuint vao1;
	GLuint colorsVBO;

};

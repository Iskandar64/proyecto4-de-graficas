#include "scene_cube.h"

#include "ifile.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#include <iostream>
#include <vector>
#include <mat4.h>

float w;
float h;

scene_cube::~scene_cube()
{
	// Borramos la memoria del ejecutable cuando
	// la escena deja de existir.
	glDeleteProgram(shader_program);
}

void scene_cube::init()
{
	std::vector<cgmath::vec3> positions;
	h = 400.0f;
	w = 400.0f;
	
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f)); //id:0
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f)); //id:1
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f) ); //id:2
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f) ); //id:3

	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f) ); //id:4
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f) ); //id:5
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f) ); //id:6
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f) ); //id:7
	
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f) ); //id:8
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f) ); //id:9
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f) ); //id:10
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f) ); //id:11
	
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f) );  //:id:12
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f) ); //id:13
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f) );  //id:14
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f) ); //id:15
	
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f) ); //id:16
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f) ); //id:17
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f) ); //id:18
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f) ); //id:19

	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f) ); //id:20
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f) ); //id:21
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f)); //id:22
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f)); //id:23
	

	std::vector<cgmath::vec3> colors; //cada vertice debe de recibir un color :v
	//Rojo cara enfrente
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f)); 
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 0.0f)); 

	//verde cara derecha
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f)); 
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f)); 
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f)); 
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 0.0f)); 

	//color cara trasera
	colors.push_back(cgmath::vec3(1.0f, 1.0f, 0.0f)); 
	colors.push_back(cgmath::vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(cgmath::vec3(1.0f, 1.0f, 0.0f));

	//color cara izquierda azul
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f)); 
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f)); 
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f)); 
	colors.push_back(cgmath::vec3(0.0f, 0.0f, 1.0f)); 

	//color cara arriba morado
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec3(1.0f, 0.0f, 1.0f));

	//color cara abajo		
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 1.0f)); 	
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 1.0f)); 	
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 1.0f)); 	
	colors.push_back(cgmath::vec3(0.0f, 1.0f, 1.0f)); 	
	

	// Indices de los vertices para dibujar 2 triangulos, formando un cuadrado a pantalla completa.
	std::vector<unsigned int> indices;

	//cara de enfrente
	indices.push_back(0); 
	indices.push_back(1); 
	indices.push_back(2); 
	indices.push_back(0); 
	indices.push_back(2); 
	indices.push_back(3);

	//cara derecha
	indices.push_back(4); 
	indices.push_back(5); 
	indices.push_back(6); 
	indices.push_back(4); 
	indices.push_back(6); 
	indices.push_back(7);

	//cara atras
	indices.push_back(8); 
	indices.push_back(9); 
	indices.push_back(10); 
	indices.push_back(8); 
	indices.push_back(10);
	indices.push_back(11);

	//cara izquierda
	indices.push_back(12); 
	indices.push_back(13);
	indices.push_back(14);
	indices.push_back(12);
	indices.push_back(14);
	indices.push_back(15);

	//cara arriba
	indices.push_back(16); 
	indices.push_back(17); 
	indices.push_back(18); 
	indices.push_back(16); 
	indices.push_back(18); 
	indices.push_back(19);

	//cara abajo
	indices.push_back(20); 
	indices.push_back(21); 
	indices.push_back(22); 
	indices.push_back(20); 
	indices.push_back(22); 
	indices.push_back(23);
	
	
	// Creacion y activacion del vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Creacion y configuracion del buffer del atributo de posicion
	glGenBuffers(1, &positionsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * 
		positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	

	glGenBuffers(1, &colorsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, colorsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * colors.size(),
		colors.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// A diferencia de los buffers de atributos, los buffers de indices deben permanecer activos. No hacemos unbind.
	glGenBuffers(1, &indicesBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) 
		* indices.size(), indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);

	// ifile es parte del codigo que yo les doy
	// El codigo fuente se encuentra en el proyecto Util
	// Su unico proposito en la vida es leer archivos de texto
	ifile shader_file;
	// El metodo read recibe la ruta al archivo de texto a leer
	// Si encuentra el archivo, intenta leerlo. En este caso,
	// estamos intentando leer un archivo llamado grid,
	// dentro de una carpeta shaders.
	shader_file.read("shaders/cubo.vert");
	// Obtenemos los contenidos leidos en el paso anterior
	// utilizando el metodo get_contents. Regresa un string
	std::string vertex_source = shader_file.get_contents();
	// OpenGL es un API de C, por lo que no trabaja con
	// strings de C++. Tenemos que hacer un cast a un tipo de
	// dato que OpenGL entienda. Podemos usar strings de C (char*)
	// o utilizar el tipo de dato definido por OpenGL (GLchar*).
	// Preferimos lo segundo.
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	// Creamos el identificador para un vertex shader,
	// utiliznado la funcion glCreateShader. La funcion
	// regresa el identificador y lo guardamos en la variable
	// vertex_shader.
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	// Utilizando la funcion glShaderSource, indicamos que queremos
	// enviar el codigo fuente del shader. La funcion espera:
	// Identificador del shader (vertex_shader)
	// Cuantos codigos fuentes queremos manadar (1)
	// El c�digo fuente (vertex_source_c)
	// La longitud del codigo fuente. Si usamos nullptr, se
	// asume que debe continuar leyendo hasta encontrar un nullptr.
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	// Compilamos el codigo fuente contenido en el shader
	// con identificador vertex_shader.
	glCompileShader(vertex_shader);

	// Revision de errores de compilacion del vertex shader
	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}


	shader_file.read("shaders/cubo.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	// El identificador del shader lo creamos pero para un 
	// shader de tipo fragment.
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	// Revision de errores de compilacion del fragment shader
	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}


	// Una vez que hemos creado los shaders necesarios,
	// creamos el manager utilizando la funcion glCreateProgram
	// que regresa el id.
	shader_program = glCreateProgram();
	// Utilizamos glAttachShader para asociar un shader con el manager
	// En este caso, shader_program es manager de vertex_shader
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);

	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "InterpolatedColor");
	//glBindAttribLocation(shader_program, 1, "VertexColor");
	//glBindAttribLocation(shader_program, 2, "VertexNormal");
	// Ejecutamos el proceso de linkeo. En esta etapa se busca
	// que los shaders puedan trabajar en conjunto y todo este
	// definido correctamente.
	glLinkProgram(shader_program);

	// Tambien deberiamos verificar que el proceso de linkeo
	// termine sin errores. Por tiempo, asumimos que el
	// resultado fue correcto.

	// Borramos los shaders, porque ya tenemos el ejecutable
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	//Enviamos la resoluci�n de la ventana al inicio de la aplicaci�n. 
	//La variable es un vector vec2. Tenemos que obtener la 
	//posicion de esa variable en el shader y asignarle el valor
	//utilizando Uniform2f
	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program,
		"iResolution");
	glUniform2f(resolution_location, 400.0f, 400.0f);
	glUseProgram(0);
}

void scene_cube::awake()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void scene_cube::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

cgmath::mat4 matrizTransformacion()
{
	
	float iTime = time::elapsed_time().count();
	float pi = 3.1415926538f;
	cgmath::mat4 rotarZ = cgmath::mat4(
		cgmath::vec4(cos(iTime * 30.0f * pi / 180.0f), 
			sin(iTime * 30.0f * pi / 180.0f), 0.0f, 0.0f),
		cgmath::vec4(-sin(iTime * 30.0f * pi / 180.0f), 
			cos(iTime * 30.0f * pi / 180.0f), 0.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 1.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	cgmath::mat4 rotarY = cgmath::mat4(
		cgmath::vec4(cos(iTime * 60.0f * pi / 180.0f), 0.0f, -sin(iTime * 60.0f * pi / 180.0f), 0.0f),
		cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
		cgmath::vec4(sin(iTime * 60.0f * pi / 180.0f), 0.0f, cos(iTime * 60.0f * pi / 180.0f), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	cgmath::mat4 rotarX = cgmath::mat4(
		cgmath::vec4(1.0f, 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, cos(iTime * 30.0f * pi / 180.0f), 
			sin(iTime * 30.0f * pi / 180.0f), 0.0f),
		cgmath::vec4(0.0f, -sin(iTime * 30.0f * pi / 180.0f), 
			cos(iTime * 30.0f * pi / 180.0f), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	cgmath::mat4 traslacion(1.0f);
	traslacion[3][2] = 10.0f;
	cgmath::mat4 modelo = rotarZ * rotarY * rotarX;
	cgmath::mat4 vista = cgmath::mat4::inverse(traslacion);
	float aspect = w / (float)h;
	float fov = 60.0f * pi / 180.0f;

	cgmath::mat4 proyeccion = cgmath::mat4(
		cgmath::vec4(1.0f / (aspect * tan(fov / 2.0f)),
			0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, (1.0f / (tan(fov / 2.0f))),
			0.0f, 0.0f ),
		cgmath::vec4(0.0f, 0.0f, -((1.0f + 1000.0f) / (1000.0f - 1.0f)),
			-1.0f),
		cgmath::vec4(0.0f, 0.0f, -((2.0f * 1000.0f * 1.0f) / (1000.0f - 1.0f)),
			 0.0f));
	//proyeccion * vista * modelo *
	return proyeccion * vista * rotarZ * rotarY * rotarX;
}

void scene_cube::mainLoop()
{
	cgmath::mat4 m4 = matrizTransformacion();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shader_program);
	

	GLuint matrix_location = glGetUniformLocation(shader_program,
		"mvp");	
	GLfloat matrix[16] = {
		m4[0][0], m4[0][1], m4[0][2], m4[0][3],
		m4[1][0], m4[1][1], m4[1][2], m4[1][3],
		m4[2][0], m4[2][1], m4[2][2], m4[2][3],
		m4[3][0], m4[3][1], m4[3][2], m4[3][3]
	};

	glUniformMatrix4fv(matrix_location, 1, GL_FALSE, matrix);
	glBindVertexArray(vao);

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
	glUseProgram(0);
}

void scene_cube::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	h = (float) height;
	w = (float)width;
	glUseProgram(shader_program);
	GLuint resolution_location =
		glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, width, height);
	glUseProgram(0);
}

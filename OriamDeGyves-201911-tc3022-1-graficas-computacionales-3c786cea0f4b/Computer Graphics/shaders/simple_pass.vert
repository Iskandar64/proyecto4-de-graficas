#version 330

in vec3 VertexPosition;
in vec3 InterpolatedColor;
out vec3 CurrentColor;
out vec4 vResult;

void main()
{
	CurrentColor = InterpolatedColor;
	vResult = vec4(VertexPosition, 1.0f);
	gl_Position = vec4(VertexPosition, 1.0f);

}
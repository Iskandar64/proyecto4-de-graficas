#version 330

uniform float time;
uniform float xMove;
uniform float yMove;

out vec4 InterpolatedColor;

void main()
{  
	  float x = floor(gl_VertexID / 2.0);
	  float y = mod(gl_VertexID + 1.0, 2.0);
  
	  float angle = x / 20.0 * radians(360.0);
	  float radius = 2.0 - y;

	  float u = radius * cos(angle);
	  float v = radius * sin(angle);
	  float xOffset;
	  float yOffset;

	  xOffset = sin(time * 2.0f);
	  yOffset = sin(time * 2.0f + xMove / 2.0) * 0.7;
	  
	  float ux = u * 2.0f - 1.0f + 4.0 * xOffset;
	  float vy = v * 2.0f - 1.0f  + 4.0 * yOffset;
	  ux = ux + 39.0 - 8.5 * xMove;
	  vy = vy + 39.0 - 8.5 * yMove;

	  vec2 xy = vec2(ux, vy) * 0.025f;
  
	  gl_Position = vec4(xy, 0.0f, 1.0f);
	  gl_PointSize = 20.0f;
	  InterpolatedColor = vec4(1.0, 0.0, 0.0, 1.0);
    
}
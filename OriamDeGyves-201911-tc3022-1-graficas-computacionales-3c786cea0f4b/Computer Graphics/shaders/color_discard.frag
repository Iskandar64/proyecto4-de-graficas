#version 330

out vec4 FragColor;

in vec3 CurrentColor;
in vec4 vResult;

void main()
{
	float result = sqrt((vResult.x * vResult.x) + (vResult.y
					* vResult.y));
	if(result < 0.25)
	{
		discard;
	}
	FragColor = vec4(CurrentColor, 1.0);
}
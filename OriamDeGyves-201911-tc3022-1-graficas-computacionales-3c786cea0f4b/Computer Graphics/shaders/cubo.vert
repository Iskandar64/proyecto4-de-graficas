#version 330

in vec3 VertexPosition;
in vec3 InterpolatedColor;
out vec3 CurrentColor;
uniform mat4 mvp;

void main()
{
	
	CurrentColor = InterpolatedColor;
	gl_Position = mvp * vec4(VertexPosition, 1.0f);
}
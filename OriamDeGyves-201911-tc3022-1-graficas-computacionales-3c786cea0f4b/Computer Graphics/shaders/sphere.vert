#version 330

uniform float time;
out vec4 InterpolatedColor;

void main()
{  
	float a = floor(gl_VertexID / 360);
	float b = mod(gl_VertexID, 360);
	
	float theta = radians(b);
	float sigma = radians(a);
	float radius = 15.0;

	float x = radius * sin(sigma) * cos(theta)  ;
	float y = radius * sin(sigma) * sin(theta) ;
	float z = radius * cos(sigma) ;
		
		
	float ux = x * 2.0f  ;
	float vy = y * 2.0f  ;
	float wz = z * 2.0f  ;
	
	vec3 xyz = vec3(ux, vy, wz) * 0.03f;
	mat4 rotarY = mat4(vec4(cos(time / 2.0), 0, -sin(time / 2.0), 0),
						vec4( 0, 1, 0, 0),
						vec4(sin(time/ 2.0), 0, cos(time/ 2.0), 0),
						vec4(0, 0, 0, 1));

	mat4 rotarZ = mat4(vec4(cos(time/ 2.0), sin(time/ 2.0), 0, 0),
						vec4(-sin(time/ 2.0), cos(time/ 2.0), 0, 0),
						vec4( 0, 0, 1, 0),							
						vec4(0, 0, 0, 1));

	mat4 rotarX = mat4(vec4(1, 0, 0, 0),
					vec4(0, cos(time/ 2.0), sin(time/ 2.0),  0),
					vec4(0, -sin(time/ 2.0), cos(time/ 2.0), 0),
					vec4(0, 0, 0, 1));
		
	gl_Position = rotarX * rotarY * rotarZ * vec4(xyz, 1.0f);
	gl_PointSize = 0.5f;
	InterpolatedColor = vec4(1.0, 0.0, 0.0, 1.0);
    
}